```
**Describe the bug**  
A clear and concise description of what the bug is.

**To Reproduce**  
Steps to reproduce the behavior:

1. Go to: ...
2. Click on: ...
3. Scroll down to: ...
4. See error

**Expected behavior**  
A clear and concise description of what you expected to happen.

**Screenshots**  
If applicable, add screenshots to help explain your problem.

**Platform**  

- OS: [Windows 10/Ubuntu 18.04/iOS 13.3/Android 10...]
- Application type: [browser (specify browser and version)/mobile app]
- Version: [v1.1.0...] (if applicable)

**Additional context**  
Add any other context about the problem here.
```
