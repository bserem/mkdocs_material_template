# Taskwarrior

[Taskwarrior](https://taskwarrior.org/) is a command line tool that helps keeping track of day-to-day tasks.  
Combined with [Bugwarrior](https://github.com/ralphbean/bugwarrior) it can add your redmine issues in your terminal!

## Bugwarrior + Redmine config

```
[general]
targets = zp_redmine
#log.level = DEBUG

[zp_redmine]
service = redmine
redmine.url = 
redmine.key = _YOUR_KEY_
redmine.user_id = _YOUR_USER_ID_
redmine.verify_ssl = False
redmine.only_if_assigned = True
redmine.issue_limit = 100
#redmine.add_tags = chiliproject
```

The last two lines (commented) are if you want to filter only one project to be
imported in taskwarrior or if you want to add custom tags.
Priority defaults to Medium in the above case.

[^1]: https://taskwarrior.org/docs/30second.html
[^2]: https://bugwarrior.readthedocs.io/en/latest/
