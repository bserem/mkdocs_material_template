# Asset Packagist

To add support for [asset-packagist.org](https://asset-packagist.org/) in a drupal-composer project you need to:

- Add in your composer.json the following snippets:

```
"repositories": [
    {
        "type": "composer",
        "url": "https://asset-packagist.org"
    }
]
```

```
    "extra": {
        "installer-types": [
            "library",
            "npm-asset",
            "bower-asset"
        ],
        "installer-paths": {
            "web/core": ["type:drupal-core"],
            "web/libraries/{$name}": [
                "type:drupal-library",
                "vendor:bower-asset",
                "vendor:npm-asset"
            ],
            "web/modules/contrib/{$name}": ["type:drupal-module"],
            "web/profiles/contrib/{$name}": ["type:drupal-profile"],
            "web/themes/contrib/{$name}": ["type:drupal-theme"],
            "drush/Commands/{$name}": ["type:drupal-drush"]
        },
```

- Require `oomphinc/composer-installers-extender:^1.1`
