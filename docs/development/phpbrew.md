# PHPBrew

- Read https://github.com/phpbrew/phpbrew for installation instructions.
- Read https://github.com/phpbrew/phpbrew/wiki/Quick-Start for a quickstart guide.
- Update versions: `phpbrew update`
- Show known versions: `phpbrew known`
- Install new versions like this: `phpbrew install 7.3.15`
- for Drupal you need at least: `+default +mb +mysql +pdo` (`+sqlite` is also need for sqlite projects)


```bash
# PHPBREW
curl -L -O https://github.com/phpbrew/phpbrew/releases/latest/download/phpbrew.phar
chmod +x phpbrew.phar

# INSTALL IN CUSTOM bin FOLDER
mkdir -p ~/Programs/bin
mv phpbrew.phar ~/Programs/bin/phpbrew
echo 'PATH=$PATH:~/Programs/bin' >> ~/.bashrc

echo '[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc' >> ~/.bashrc
. ~/.bashrc
phpbrew init
phpbrew install php-7.3.15 +default +md +mysql +pdo +sqlite
php switch php-7.3.15
php use php-7.3.15
which php
echo "Type `phpbrew off` to switch to system php temporarily!"
```
