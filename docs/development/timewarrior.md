# Timewarrior

Connect timewarrior with taskwarrior:
```
   cp /usr/share/doc/timewarrior/ext/on-modify.timewarrior .task/hooks/
   chmod +x ~/.task/hooks/on-modify.timewarrior
   task diagnostics
```

Now, when you run `task start` and `task stop` timewarrior will be automatically
triggered and start counting for you.
