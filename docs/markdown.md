# Markdown Syntax

## Basics

*Emphasized Text*  
_Alternative Emphasized Text_

**Stong Text**
__Alternative Strong Text Syntax__

~~~
*Emphasized Text*  
_Alternative Emphasized Text_

**Stong Text**
__Alternative Strong Text Syntax__
~~~


## Headers

## Header 2
### Header 3
#### Header 4


~~~
## Header 2
### Header 3
#### Header 4
~~~


## Line & Paragraph breaks

To force a line return,   
place two empty spaces at the end of a line.
~~~
Line break is  
when you add 2 spaces in the end of the line.
~~~

Paragraph break is 

when you add 2 line breaks
~~~
Paragraph break is 

when you add 2 line breaks
~~~

If you just add one line break
nothing will happen

~~~
If you just add one line break
nothing will happen
~~~


## Links

Short Syntax: <http://some-url.ch/>

Full Syntax: [Text URL](http://some-url.ch/ "With a Title")

Link Reference can also be like [this][1].
[1]: http://en.wikipedia.org/wiki/Markdown        "Markdown"

~~~
Short Syntax: <http://some-url.ch/>

Full Syntax: [Text URL](http://some-url.ch/ "With a Title")

Link Reference can also be like [this][1].
[1]: http://en.wikipedia.org/wiki/Markdown        "Markdown"
~~~


## Lists

* Item 1
* Item 2
    * Nested Item needs exactly 4 spaces indent

~~~
* Item 1
* Item 2
    * Nested Item needs exactly 4 spaces indent
~~~

_You may also use '-' or '+' instead of '*'._

or if you want numbered:

1. Item
2. Item

~~~
1. Item
2. Item
~~~

## Quoted Text

> This is a quoted text

~~~
> This is a quoted text
~~~


## Tables

| Project Name      | Drupal | URL   |
| ----------------- | ------ | ----- |
| `education21`     | 7      | education21.ch |
| `Pronatura`       | 8      | pronatura.ch |


~~~
| Project Name      | Drupal | URL   |
| ----------------- | ------ | ----- |
| `education21`     | 7      | education21.ch |
| `Pronatura`       | 8      | pronatura.ch |
~~~

## Code / No Format

Add  ~~~ or ``` before & after the code in new lines.
