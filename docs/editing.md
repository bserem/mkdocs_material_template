# Documendation Editing

## Online Editing

You can preview and edit any page online at any time on Bitbucket.
The pen icon on the top right of any page, here in the docs, acts as a quick
link towards the bitbucket file for this page.

Example:
https://bitbucket.org/MY_REPO/src/master/docs/index.md
Click "Edit", edit the file, and then click "Commit".

The link to the repository is also on the top right of every page, next to the search.

## Local Editing

Clone and edit it with your favorite editor.
You can install mkdocs to preview or build the documendation yourself, as described below.

## Deploying
Pages are automatically deployed when commited/pushed to bitbucket!

## Installing MkDocs (optional)
!!! tip
    Make sure python and pip are on version 3, or use the python3/pip3 executables.

### 1) MkDocs

* [Python](https://www.python.org/)
* [Python Pip](https://pypi.org/project/pip/)
* [MkDocs](https://www.mkdocs.org/)

### 2) Plugins

Themes & Plugins used in our documendation project:

* [Material Theme](https://github.com/squidfunk/mkdocs-material)
* [Awesome Pages Plugin](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin)
* [git-revision-date](https://github.com/zhaoterryy/mkdocs-git-revision-date-plugin)

#### Styling text
Admonition (styled tips): https://squidfunk.github.io/mkdocs-material/extensions/admonition/
Footnotes: https://squidfunk.github.io/mkdocs-material/extensions/footnotes/

~~~
pip install mkdocs-material mkdocs-awesome-pages-plugin mkdocs-git-revision-date-plugin
~~~

or

~~~
pip3 install mkdocs-material mkdocs-awesome-pages-plugin mkdocs-git-revision-date-plugin
~~~

## Using MkDocs

### Build

~~~
mkdocs build
~~~

or

~~~
python3 -m mkdocs build
~~~


### Live Editing

~~~
mkdocs serve
~~~

or

~~~
python3 -m mkdocs serve
~~~
