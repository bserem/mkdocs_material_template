#!/bin/bash

tmux new-session -d -s docs

tmux new-window -t docs:9 -n server
tmux send-keys -t docs:9 "mkdocs serve" C-m
tmux new-window -t docs:0 -n groot

tmux new-window -t docs:1 -n docs
tmux send-keys -t docs:1 "cd docs" C-m

tmux select-window -t docs:1
tmux attach-session -d -t docs
